import { gql } from 'apollo-server';
import { typeDefs as scalarTypeDefs } from 'graphql-scalars';

const arguTypeDefs = gql`
    enum CacheControlScope {
        PUBLIC
        PRIVATE
    }

    directive @cacheControl(
        maxAge: Int
        scope: CacheControlScope
        inheritMaxAge: Boolean
    ) on FIELD_DEFINITION | OBJECT | INTERFACE | UNION

    type TenantDescription {
        name: String!
        location: String!
    }

    type Organization {
        id: ID!
        rdfType: [String]!
        dateCreated: DateTime
        datePublished: DateTime
        name: String
        text: String
    }

    enum UsageTypes {
        CONTENT_PHOTO
        COVER_PHOTO
        PROFILE_PHOTO
        ATTACHMENT
    }

    enum ContentSource {
        LOCAL
        REMOTE
    }
    
    type Person {
        id: ID!
        rdfType: [String]
        image: MediaObject
        acceptTerms: Boolean
        acceptedTerms: Boolean
        introFinished: Boolean
        name: String
        public: Boolean
        votesPublic: Boolean
    }

    type MediaObject {
        id: ID!
        rdfType: [String]
        creator: Person

        uploadDate: DateTime
        datePublished: DateTime
        organization: Organization
        contentUrl: String
        encodingFormat: String
        filename: String
        thumbnail: ID
        imagePositionY: Int
        fileUsage: UsageTypes
        copyUrl: ID
        contentSource: ContentSource
        imgUrl64x64: String
        imgUrl256x256: String
        imgUrl568x400: String
        imgUrl1500x2000: String
        actionsMenu: ID
        isPartOf: ID
        destroyAction: ID
        potentialAction: ID
    }

    type Forum {
        id: ID!
        rdfType: [String]!
        dateCreated: DateTime
        datePublished: DateTime
        name: String
        organization: ID
        grantedSets: ID
        grantedGroups: ID
        trashed: Boolean
        isDraft: Boolean
        lastActivityAt: DateTime
        pinned: Boolean
        shortname: String
        dateModified: DateTime
        widgets: ID
        description: String
        discoverable: Boolean
        showHeader: Boolean
        followsCount: Int
        hideHeader: Boolean
        isPartOf: ID
        creator: Person
        coverPhoto: MediaObject
        locationQuery: ID
        childrenPlacements: ID
        image: MediaObject
        grants: ID
        creative_works: ID
        customActions: ID
        surveys: ID
        projects: ID
        budgetShops: ID
        actionsMenu: ID
        followMenu: ID
        shareMenu: ID
        tabsMenu: ID
        blogPosts: ID
        discussions: ID
        motions: ID
        questions: ID
        polls: ID
        swipeTools: ID
        topics: ID
        transferAction: ID
        destroyAction: ID
        followNewsAction: ID
        followReactionsAction: ID
        followNeverAction: ID
        updateAction: ID
    }

    type Motion @cacheControl(maxAge: 240, scope: PUBLIC) {
        id: ID!
        rdfType: [String]!
        dateCreated: DateTime
        datePublished: DateTime
        
        coverPhoto: MediaObject

        name: String
        organization: Organization
        grantedSets: String
        grantedGroups: String
        trashed: Boolean
        isDraft: Boolean
        lastActivityAt: DateTime
        pinned: String
        shortname: String
        text: String
        followsCount: Int
        commentsCount: Int
        markAsImportant: Boolean
        important: Boolean
        invertArguments: String
        proArgumentsCount: Int
        conArgumentsCount: Int
        dateModified: String
        favoriteAction: String
        creator: Person
        attachments: ID
        blogPosts: ID
        comment: [Comment]
        arguPublication: ID
        locationQuery: ID
        actionsMenu: ID
        followMenu: ID
        shareMenu: ID
        tabsMenu: ID
        proArguments: ID
        conArguments: ID
        argumentColumns: ID
        voteEvents: ID
        voteableVoteEvent: ID
        optionsVocab: ID
        transferAction: ID
        potentialAction: String
        moveAction: ID
        updateAction: ID
        destroyAction: ID
        trashAction: ID
        untrashAction: ID
        followNewsAction: ID
        followReactionsAction: ID
        followNeverAction: ID
        createAction: String
    }

    type Comment {
        id: ID!
        rdfType: [String]!
        dateCreated: DateTime
        datePublished: DateTime
        text: String
    }

    type Query {
        comments(parent: String!, page: Int): [Comment]
        comment(id: String!): Comment

        discussions(parent: String!, page: Int): [ID]

        forums(parent: String!, page: Int): [Forum]
        forum(id: String!): Forum

        mediaObjects(parent: String!, page: Int): [MediaObject]
        mediaObject(id: String!): MediaObject

        motions(parent: String!, page: Int): [Motion]
        motion(id: String!): Motion

        organization: Organization

        person(id: String!): Person

        tenants: [TenantDescription]
        tenantByName(name: String!): TenantDescription
    }
`;

export const typeDefs = [
  arguTypeDefs,
  ...scalarTypeDefs,
];
