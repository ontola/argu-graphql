/* eslint-disable @typescript-eslint/explicit-module-boundary-types */

import { Organization } from '../argu/organizations';

export const organizationResolvers = {
  organization(_, __, { dataSources }): Promise<Organization> {
    return dataSources.organizations.getRecord('');
  },
};
