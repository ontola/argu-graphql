/* eslint-disable @typescript-eslint/explicit-module-boundary-types */

import { Motion } from '../argu/motions';
import { trimId } from '../empjson';

export const motionResolvers = {
  motion(_, { id }, { dataSources }): Promise<Motion> {
    return dataSources.motions.getRecord(id);
  },

  async motions(_, { parent, page }, { dataSources, tenant }): Promise<string[]> {
    const motionIds = await dataSources.motions.getMotions(parent, page ?? 1);

    return motionIds.map((id) => dataSources.motions.getRecord(trimId(tenant, id)));
  },
};
