/* eslint-disable @typescript-eslint/explicit-module-boundary-types */

import { Person } from '../argu/persons';

export const personResolvers = {
  person(_, { id }, { dataSources }): Promise<Person> {
    return dataSources.persons.getRecord(id);
  },
};
