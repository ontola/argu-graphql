/* eslint-disable @typescript-eslint/explicit-module-boundary-types */

import { MediaObject } from '../argu/mediaObjects';

export const mediaObjectResolvers = {
  mediaObject(_, { id }, { dataSources }): Promise<MediaObject> {
    return dataSources.mediaObjects.getRecord(id);
  },

  async mediaObjects(_, { parent, page }, { dataSources }): Promise<string[]> {
    const mediaObjectIds = await dataSources.mediaObjects.getMediaObjects(parent, page ?? 1);

    return mediaObjectIds.map((id) => dataSources.mediaObjects.getRecord(id.split('/media_objects/').pop()));
  },
};
