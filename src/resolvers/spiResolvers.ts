/* eslint-disable @typescript-eslint/explicit-module-boundary-types */

import { TenantDescription } from '../arguSpi';

export const spiResolvers = {
  tenantByName(_, { name }, { dataSources }): Promise<TenantDescription> {
    return dataSources.spi.getTenantByName(name);
  },

  tenants(_, __, { dataSources }): Promise<TenantDescription[]> {
    return dataSources.spi.getTenants();
  },
};
