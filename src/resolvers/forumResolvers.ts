/* eslint-disable @typescript-eslint/explicit-module-boundary-types */

import { Forum } from '../argu/forum';

export const forumResolvers = {
  forum(_, { id }, { dataSources }): Promise<Forum> {
    return dataSources.forums.getRecord(id);
  },

  async forums(_, { parent, page }, { dataSources }): Promise<string[]> {
    const forumIds = await dataSources.forums.getForums(parent, page ?? 1);

    return forumIds.map((id) => dataSources.forums.getRecord(id.split('/f/').pop()));
  },
};
