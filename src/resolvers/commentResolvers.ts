/* eslint-disable @typescript-eslint/explicit-module-boundary-types */

import { Motion } from '../argu/motions';
import { trimId } from '../empjson';

export const commentResolvers = {
  comment(_, { id }, { dataSources }): Promise<Motion> {
    return dataSources.comments.getRecord(id);
  },

  async comments(_, { parent, page }, { dataSources, tenant }): Promise<string[]> {
    const commentIds = await dataSources.comments.getComments(parent, page ?? 1);

    return commentIds.map((id) => dataSources.comments.getRecord(trimId(tenant, id)));
  },
};
