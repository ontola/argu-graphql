import { Argu } from './argu';

export class Discussions extends Argu {
  fieldMapping = {};

  async getDiscussions(parent: string, page = 1): Promise<string[]> {
    return this.getCollectionMembers(parent, 'discussions', page);
  }
}

