import {
  bool,
  id as idParser,
  str,
} from '../empjson';

import { Argu } from './argu';
import {
  crudable,
  imageable,
  recordBase,
} from './enhancements';

export type Person = Record<string, unknown>;

export class Persons extends Argu {
  fieldMapping = {
    id: idParser,
    ...recordBase,
    ...crudable,
    ...imageable,

    acceptTerms: bool,
    acceptedTerms: bool,
    introFinished: bool,
    name: str,
    public: bool,
    votesPublic: bool,
  };

  async getPersons(parent: string, page = 1): Promise<string[]> {
    return this.getCollectionMembers(parent, 'u', page);
  }
}
