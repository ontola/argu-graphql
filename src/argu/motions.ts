import {
  bool,
  str,
} from '../empjson';

import { Argu } from './argu';
import {
  argumentable,
  attachable,
  commentable,
  coverPhotoable,
  crudable,
  edge,
  followable,
  grantable,
  movable,
  pinnable,
  publishable,
  recordBase,
  trashable,
  votable,
} from './enhancements';

// TODO: see if it's possible to generate TS declarations from graphql schema
export type Motion = Record<string, unknown>;

export class Motions extends Argu {
  fieldMapping = {
    ...recordBase,
    ...edge,
    ...argumentable,
    ...attachable,
    ...commentable,
    ...coverPhotoable,
    ...crudable,
    ...followable,
    ...grantable,
    ...publishable,
    ...pinnable,
    ...movable,
    ...trashable,
    ...votable,
    actionsMenu: str,
    blogPosts: str,
    favoriteAction: str,
    important: bool,
    locationQuery: str,
    markAsImportant: bool,
    name: str,
    optionsVocab: str,
    potentialAction: str,
    shareMenu: str,
    shortname: str,
    tabsMenu: str,
    text: str,
    transferAction: str,
  };

  async getMotions(parent: string, page = 1): Promise<string[]> {
    return this.getCollectionMembers(parent, 'm', page);
  }
}
