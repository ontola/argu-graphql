import { str } from '../empjson';

import { Argu } from './argu';
import {
  crudable,
  recordBase,
  trashable,
} from './enhancements';

export type Comment = Record<string, unknown>;

export class Comments extends Argu {
  fieldMapping = {
    ...recordBase,
    ...crudable,
    ...trashable,
    text: str,
  };
  async getComments(parent: string, page = 1): Promise<string[]> {
    return this.getCollectionMembers(parent, 'c', page);
  }
}
