import { RESTDataSource, RequestOptions } from 'apollo-datasource-rest';

import { EmpJsonValue, ValueParser } from '../empjson';
import { dataServiceUrl } from '../env';
import mapping from '../mapping.json';

export type FieldMapping = Record<string, ValueParser<unknown>>;

export abstract class Argu extends RESTDataSource {
  protected abstract fieldMapping: FieldMapping;

  constructor() {
    super();
    this.baseURL = dataServiceUrl;
  }

  willSendRequest(request: RequestOptions): void {
    const tenant = new URL(this.context.tenant);

    request.headers.set('accept', 'application/empathy+json');
    request.headers.set('authority', tenant.host);
    request.headers.set('host', tenant.hostname);
    request.headers.set('origin', tenant.origin);
    request.headers.set('website-iri', tenant.toString());
    request.headers.set('x-forwarded-proto', 'https');
    request.headers.set('x-forwarded-ssl', 'on');
  }

  protected async getCollectionMembers(parent: string, subCollection: string, page: number): Promise<string[]> {
    const iri = new URL(`${this.context.tenant}${parent}/${subCollection}?page=${page}`);
    const data = await this.get(iri.toString().split(iri.origin).join(''));
    const slice = JSON.parse(data);
    iri.hash = 'members';
    const members = slice[iri.toString()];

    return Object.entries<EmpJsonValue>(members ?? {})
      .filter(([it]) => it.includes('#_'))
      .map(([, it]) => it.v);
  }

  protected async getRecord<T>(id: string): Promise<T | null> {
    const iri = new URL(`${this.context.tenant}${id}`);
    const data = await this.get(iri.pathname);
    const slice = JSON.parse(data);
    const record = slice[iri.toString()];

    if (!record) {
      return null;
    }

    return Object.entries(this.fieldMapping).reduce((acc, [field, convert]) => ({
      ...acc,
      [field]: convert(this.context, record[mapping[field]]),
    }), {}) as T;
  }
}
