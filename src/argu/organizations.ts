import { str } from '../empjson';

import { Argu } from './argu';
import {
  crudable,
  edge,
  grantable,
  recordBase,
  trashable,
} from './enhancements';

export type Organization = Record<string, unknown>;

export class Organizations extends Argu {
  fieldMapping = {
    ...recordBase,
    ...edge,
    ...crudable,
    ...grantable,
    ...trashable,
    name: str,
    text: str,
  };
}
