
import {
  date,
  enumeration,
  id as idParser,
  int,
  str,
} from '../empjson';

import { Argu } from './argu';
import { crudable, recordBase } from './enhancements';

// TODO: see if it's possible to generate TS declarations from graphql schema
export type MediaObject = Record<string, unknown>;

const fileUsageMapping = {
  attachment: 'ATTACHMENT',
  content_photo: 'CONTENT_PHOTO',
  cover_photo: 'COVER_PHOTO',
  profile_photo: 'PROFILE_PHOTO',
};

const contentSourceMapping = {
  local: 'LOCAL',
  remote: 'REMOTE',
};

export class MediaObjects extends Argu {
  fieldMapping = {
    id: idParser,
    ...recordBase,
    ...crudable,

    actionsMenu: idParser,
    contentSource: enumeration(contentSourceMapping),
    contentUrl: str,
    copyUrl: idParser,
    datePublished: date,
    destroyAction: idParser,
    encodingFormat: str,
    fileUsage: enumeration(fileUsageMapping),
    filename: str,
    imagePositionY: int,
    imgUrl64x64: str,
    imgUrl256x256: str,
    imgUrl568x400: str,
    imgUrl1500x2000: str,
    isPartOf: idParser,
    organization: idParser,
    potentialAction: idParser,
    thumbnail: idParser,
    uploadDate: date,
  };

  async getMediaObjects(parent: string, page = 1): Promise<string[]> {
    return this.getCollectionMembers(parent, 'media_objects', page);
  }
}
