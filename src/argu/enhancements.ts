/**
 * This file contains a sets of associated properties.
 * See the argu backend for their structure and meaning.
 */

import {
  array,
  bool,
  date,
  id,
  int,
  str,
} from '../empjson';

export const recordBase = {
  id: id,
  organization: id,
  rdfType: array(id),
};

export const edge = {
  isPartOf: id,
  lastActivityAt: date,
};

export const argumentable = {
  argumentColumns: id,
  conArguments: id,
  conArgumentsCount: int,
  invertArguments: bool,
  proArguments: id,
  proArgumentsCount: int,
};

export const attachable = {
  attachments: id,
};

export const blogPostable = {
  blogPosts: id,
};

export const childrenPlaceable = {
  childrenPlacements: id,
};

export const commentable = {
  comment: array(id),
  commentsCount: int,
};

export const coverPhotoable = {
  coverPhoto: id,
};

export const crudable = {
  createAction: id,
  creator: id,
  dateCreated: date,
  dateModified: date,
  destroyAction: id,
  updateAction: id,
};

export const followable = {
  followMenu: id,
  followNeverAction: id,
  followNewsAction: id,
  followReactionsAction: id,
  followsCount: int,
};

export const grantable = {
  grantedGroups: id,
  grantedSets: id,
  grants: id,
  permissionGroups: id,
  permissions: id,
};

export const imageable = {
  image: id,
};

export const motionable = {
  motions: id,
};

export const nameable = {
  name: str,
};

export const publishable = {
  arguPublication: id,
  datePublished: date,
  isDraft: bool,
};

export const pinnable = {
  pinned: bool,
};

export const movable = {
  moveAction: str,
};

export const questionable = {
  questions: id,
};

export const shareable = {
  shareMenu: id,
};

export const topicable = {
  topics: id,
};

export const trashable = {
  trashAction: id,
  trashed: bool,
  untrashAction: id,
};

export const transferable = {
  transferAction: id,
};

export const votable = {
  voteEvents: id,
  voteableVoteEvent: id,
};
