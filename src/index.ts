import { ApolloServer } from 'apollo-server';
import { ApolloServerPluginLandingPageLocalDefault as apolloServerPluginLandingPageLocalDefault } from 'apollo-server-core';
import responseCachePlugin from 'apollo-server-plugin-response-cache';

import { dataSources } from './dataSources';
import { resolvers } from './resolvers';
import { typeDefs } from './schema';

const server = new ApolloServer({
  cache: 'bounded',
  context: ({ req }) => {
    const tenantHeader = req.headers['website-iri'];
    const tenantValue = Array.isArray(tenantHeader) ? tenantHeader[0] : tenantHeader;

    return {
      tenant: tenantValue.replace(/\/$/, ''),
    };
  },
  csrfPrevention: true,
  dataSources,
  plugins: [
    responseCachePlugin,
    apolloServerPluginLandingPageLocalDefault({ embed: true }),
  ],
  resolvers,
  typeDefs,
});

server.listen().then(({ url }) => {
  // eslint-disable-next-line no-console
  console.log(`🚀  Server ready at ${url}`);
});
