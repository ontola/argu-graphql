export interface ParsingContext {
  tenant: string;
}

export interface EmpJsonValue {
  type: string;
  v: string;
}

export type ValueParser<T> = (context: ParsingContext, v: EmpJsonValue) => T;

export const array = <T> (valueParser: ValueParser<T>): (ctx: ParsingContext, v: EmpJsonValue) => T[] =>
  (ctx: ParsingContext, value: EmpJsonValue | undefined): T[] => [valueParser(ctx, value)];

export const trimId = (tenant: string, id: string): string => {
  const trimmed = id.split(tenant).pop();

  if (trimmed === '') {
    return '/';
  }

  return trimmed;
};

export const bool: ValueParser<boolean> = (_: ParsingContext, value: EmpJsonValue | undefined): boolean | undefined => {
  if (!value) {
    return undefined;
  }

  return value.v === 'true';
};

export const int: ValueParser<number> = (_: ParsingContext, value: EmpJsonValue | undefined): number | undefined => {
  if (!value) {
    return undefined;
  }

  return Number.parseInt(value.v, 10);
};

export const id: ValueParser<string> = (ctx: ParsingContext, value: EmpJsonValue | undefined): string | undefined => {
  if (!value || value.v === undefined) {
    return undefined;
  }

  return trimId(ctx.tenant, value.v);
};

export const str: ValueParser<string> = (_: ParsingContext, value: EmpJsonValue | undefined): string | undefined => {
  if (!value) {
    return undefined;
  }

  return value.v;
};

export const enumeration: (mapping: Record<string, string>) => ValueParser<string> = (mapping: Record<string, string>) => (_: ParsingContext, value: EmpJsonValue | undefined): string | undefined => {
  if (!value) {
    return undefined;
  }

  return mapping[value.v];
};

export const date: ValueParser<string> = (_: ParsingContext, value: EmpJsonValue | undefined): string | undefined => {
  if (!value?.v) {
    return undefined;
  }

  return value.v;
};
