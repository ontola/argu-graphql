import { RESTDataSource } from 'apollo-datasource-rest';

import { dataServiceUrl } from './env';

export interface TenantDescription {
  name: string;
  location: string;
}

export class ArguSpi extends RESTDataSource {
  constructor() {
    super();
    this.baseURL = `${dataServiceUrl}_public/spi`;
  }

  async getTenants(): Promise<TenantDescription[]> {
    const data = await this.get('/tenants');

    return data.sites;
  }

  async getTenantByName(name: string): Promise<TenantDescription> {
    const data = await this.get('/tenants');

    return data.sites.find((it) => it.name === name);
  }
}
