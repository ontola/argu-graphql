import { RESTDataSource } from 'apollo-datasource-rest';

import { Comments } from './argu/comments';
import { Discussions } from './argu/discussions';
import { Forums } from './argu/forum';
import { MediaObjects } from './argu/mediaObjects';
import { Motions } from './argu/motions';
import { Organizations } from './argu/organizations';
import { Persons } from './argu/persons';
import { ArguSpi } from './arguSpi';

export const dataSources = (): Record<string, RESTDataSource> => ({
  comments: new Comments(),
  discussions: new Discussions(),
  forums: new Forums(),
  mediaObjects: new MediaObjects(),
  motions: new Motions(),
  organizations: new Organizations(),
  persons: new Persons(),
  spi: new ArguSpi(),
});
