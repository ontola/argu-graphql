/* eslint-disable @typescript-eslint/no-magic-numbers */

import {
  array,
  bool,
  date,
  enumeration,
  id,
  int,
  str,
} from '../empjson';

describe('empjson', () => {
  const ctx = {
    tenant: 'http://localhost/',
  };

  describe('str', () => {
    it('handles undefined', () => {
      expect(str(ctx, undefined)).toEqual(undefined);
    });

    it('parses empty value', () => {
      expect(str(ctx, {
        type: 's',
        v: '',
      })).toEqual('');
    });

    it('parses value', () => {
      expect(str(ctx, {
        type: 's',
        v: 'string value',
      })).toEqual('string value');
    });
  });

  describe('id', () => {
    it('handles undefined', () => {
      expect(id(ctx, undefined)).toEqual(undefined);
    });

    it('parses empty value', () => {
      expect(id(ctx, {
        type: 's',
        v: '',
      })).toEqual('/');
    });

    it('strips prefix', () => {
      expect(id(ctx, {
        type: 's',
        v: 'http://localhost/resource/4',
      })).toEqual('resource/4');
    });
  });

  describe('date', () => {
    const current = new Date().toISOString();
    it('handles undefined', () => {
      expect(date(ctx, undefined)).toEqual(undefined);
    });

    it('discards empty value', () => {
      expect(date(ctx, {
        type: 'dt',
        v: '',
      })).toEqual(undefined);
    });

    it('parses value', () => {
      expect(date(ctx, {
        type: 'dt',
        v: current,
      })).toEqual(current);
    });
  });

  describe('int', () => {
    it('handles undefined', () => {
      expect(int(ctx, undefined)).toEqual(undefined);
    });

    it('parses zero', () => {
      expect(int(ctx, {
        type: 'i',
        v: '0',
      })).toEqual(0);
    });

    it('parses numbers', () => {
      expect(int(ctx, {
        type: 'i',
        v: '12',
      })).toEqual(12);
    });
  });

  describe('bools', () => {
    it('handles undefined', () => {
      expect(bool(ctx, undefined)).toEqual(undefined);
    });

    it('parses true', () => {
      expect(bool(ctx, {
        type: 'b',
        v: 'true',
      })).toEqual(true);
    });

    it('parses false', () => {
      expect(bool(ctx, {
        type: 'b',
        v: 'false',
      })).toEqual(false);
    });
  });

  describe('enumeration', () => {
    const mapping = {
      'http://example.com/a': 'A',
      'http://example.com/b': 'B',
    };

    it('handles undefined', () => {
      const bound = enumeration(mapping);

      expect(bound(ctx, undefined)).toEqual(undefined);
    });

    it('converts the value', () => {
      const bound = enumeration(mapping);

      expect(bound(ctx, {
        type: 'id',
        v: 'http://example.com/a',
      })).toEqual('A');
    });
  });

  describe('array', () => {
    it('uses the function', () => {
      expect(array((_, { v }) => v + '.')(ctx, {
        type: '',
        v: '2',
      })).toEqual(['2.']);
    });
  });
});
