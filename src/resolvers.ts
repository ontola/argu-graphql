/* eslint-disable @typescript-eslint/explicit-module-boundary-types */

import { resolvers as scalarResolvers } from 'graphql-scalars';

import { trimId } from './empjson';
import { commentResolvers } from './resolvers/commentResolvers';
import { forumResolvers } from './resolvers/forumResolvers';
import { mediaObjectResolvers } from './resolvers/mediaObjectResolvers';
import { motionResolvers } from './resolvers/motionResolvers';
import { organizationResolvers } from './resolvers/organizationResolvers';
import { personResolvers } from './resolvers/personResolvers';
import { spiResolvers } from './resolvers/spiResolvers';

const crudable = {
  async creator(record, _, { dataSources }) {
    return await dataSources.persons.getRecord(record.creator);
  },
};

const coverPhotoable = {
  coverPhoto(record, _, { dataSources }) {
    if (!record.coverPhoto) {
      return null;
    }

    return dataSources.mediaObjects.getRecord(record.coverPhoto);
  },
};

const imageable = {
  image(record, _, { dataSources }) {
    if (!record.image) {
      return null;
    }

    return dataSources.mediaObjects.getRecord(record.image);
  },
};

const commentable = {
  async comment(motion, _, { dataSources, tenant }) {
    const commentIds = await dataSources.comments.getComments(motion.id);

    return commentIds.map((id) => dataSources.comments.getRecord(trimId(tenant, id)));
  },
};

export const resolvers = {
  ...scalarResolvers,

  Forum: {
    ...crudable,
    ...coverPhotoable,
    ...imageable,
  },

  MediaObject: {
    ...crudable,
  },

  Motion: {
    ...crudable,
    ...coverPhotoable,
    ...commentable,

    organization(_, __, { dataSources }) {
      return dataSources.organizations.getRecord();
    },
  },

  Query: {
    discussions(_, { parent, page }, { dataSources }): Promise<string[]> {
      return dataSources.discussions.getDiscussions(parent, page ?? 1);
    },

    ...commentResolvers,
    ...forumResolvers,
    ...motionResolvers,
    ...mediaObjectResolvers,
    ...organizationResolvers,
    ...personResolvers,
    ...spiResolvers,
  },
};
